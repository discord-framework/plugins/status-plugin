add_requires(
    "spdlog",
    "fmt",
    "libpqxx",
    "pcre",
    "cpr",
    "args",
    "nlohmann_json",
    "json-schema-validator",
    "dpp",
    {
        configs = {
            shared = true
        }
    }
)

function load_packages()
    add_packages("spdlog", "fmt", "pcre", "dl", "dpp", "libpqxx", "cpr", "args", "json-schema-validator", "nlohmann_json")
end

function load_autoformat()
    before_build(function(target)
        os.exec("bash -c \"find . -iname *.cpp -iname *.h | xargs clang-format\"")
    end)
end

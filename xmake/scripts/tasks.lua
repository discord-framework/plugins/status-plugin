compose_path = "/usr/bin/docker-compose"

task("up")
    -- Set the run script
    on_run(function()
        if (os.exists(compose_path)) then
            os.execv(compose_path, { "up" })
        else
            print("docker-compose not found")
            return 1
        end
    end)

    set_menu{
      -- Setting menu usage
      usage = "xmake up",
      -- Setup menu description
      description = "Boot the docker environment",
      -- Setup menu options
      {}
    }
task_end()

task("down")
    -- Set the run script
    on_run(function()
        if (os.exists(compose_path)) then
                os.execv(compose_path, {"down"})
        else
            print("docker-compose not found")
            return 1
        end
    end)

    set_menu{
        -- Setting menu usage
        usage = "xmake up",
        -- Setup menu description
        description = "Shutdown the docker environment",
        -- Setup menu options
        {}
    }
task_end()

package("libpqxx")
    set_homepage("https://github.com/jtv/libpqxx")
    set_description("The official C++ client API for PostgreSQL")
    set_license("BSD-3-Clause License")

    add_urls("https://github.com/jtv/libpqxx.git")
    add_versions("v7.6.0", "221ddc8be329bafb376a3d83b9cd257fd52fc7b7")
    add_versions("v7.6.1", "e694c5a9889039ebe9bfbaa47b22c0ce117057db")
    
    add_deps("cmake")

    add_syslinks("pq")

    on_install(function (package)
        local configs = {}
        table.insert(configs, "-DCMAKE_BUILD_TYPE=" .. (package:debug() and "Debug" or "Release"))
        table.insert(configs, "-DBUILD_SHARED_LIBS=" .. (package:config("shared") and "ON" or "OFF"))
        import("package.tools.cmake").install(package, configs)
    end)

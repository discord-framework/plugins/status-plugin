#pragma once

#include <concepts>
#include <fmt/format.h>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <discordframework/export.h>
#include <discordframework/services/service.h>

namespace DiscordFramework {
class Service;
class Bot;

/**
 * @brief Service manager
 * Manage singletons of each services than need to be shared everywhere
 * in the bot.
 */
class EXPORT_DF ServiceManager {
  /**
   * @brief Custom type to represent the service dictionary
   *
   */
  typedef std::map<std::string, Service *> ServicesList;

  struct ServiceManagerBuilder {
  private:
    ServicesList m_list;
    std::vector<std::string> m_order;

  public:
    ServiceManagerBuilder();
    ~ServiceManagerBuilder();
    ServiceManagerBuilder &add_native_service(const std::string &name,
                                              Service *s);
    ServiceManager build();
  };

public:
  /**
   * @brief When the service manager is created, it will load all
   * native services to directly provied them to the application.
   *
   * @param s List of the native service to boot with
   * @return ServiceManager The service manager
   */
  static ServiceManagerBuilder create();

  /**
   * @brief Boot all navite services
   */
  void boot();

  /**
   * @brief Construct a new Service Manager object
   *
   * @param s
   */
  ServiceManager(const ServiceManager &s);

  /**
   * @brief Prevent the object to be copied
   */
  ServiceManager &operator=(const ServiceManager &s) = delete;

  ~ServiceManager();

  /**
   * @brief Add a new service to the manager
   *
   * @param name The name of the service
   * @param service The service to add
   * @return ServiceManager& The service manager
   */
  ServiceManager &add(const std::string &name, Service *service);
  /**
   * @brief Remove a service from the manager
   *
   * @param name The name of the service
   * @return ServiceManager& The service manager
   */
  ServiceManager &remove(const std::string &name);

  /**
   * @brief Check if a service is already loaded
   *
   * @param name The name of the service
   * @return true If true, that mean the service exists and is loaded
   */
  bool has(const std::string &name);

  /**
   * @brief Get a service from his name
   *
   * @tparam T The type of the service
   * @param name The name of the service
   * @return std::shared_ptr<T> The service
   */
  template <std::derived_from<Service> T> T *get(const std::string &name) {
    return reinterpret_cast<T *>(this->getService(name));
  };

  Bot *bot() const;

private:
  friend class Bot;

  /**
   * @brief Constructor
   *
   * @param s List of the native service to boot with
   */
  ServiceManager(const ServicesList &s = {},
                 std::vector<std::string> order = {});

  /**
   * @brief The list of the services
   */
  ServicesList m_services{};
  /**
   * @brief The list of the native services
   */
  ServicesList m_native_services{};

  std::vector<std::string> m_native_order{};

  Bot *m_bot;

  Service *getService(const std::string &name);
};
} // namespace DiscordFramework

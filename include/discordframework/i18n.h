#pragma once

#include <discordframework/export.h>
#include <filesystem>
#include <fstream>
#include <string>
#include <unordered_map>

#include <dpp/nlohmann/json.hpp>
#include <fmt/core.h>

#define i18n_format fmt::format

typedef nlohmann::json json;

/**
 * @brief Translation tool based on dictionnaries.
 */
class EXPORT_DF i18n {
  typedef std::unordered_map<std::string,
                             std::unordered_map<std::string, std::string>>
      dictionary;

public:
  i18n(const i18n &) = delete;
  i18n(i18n &) = delete;
  i18n &operator=(const i18n &) = delete;
  i18n &operator=(i18n &) = delete;

  /**
   * Initialize the default configuration
   * @param localePath Locales folder containing all your translations
   * @param locale Used language
   * @param defaultLocale Default used language
   * @param defaultNS Namespace used by default for translations
   * @param localeExtension Locales file extensions
   */
  static void Init(std::filesystem::path localePath = "locales",
                   const std::string &locale = "en-US",
                   const std::string &defaultLocale = "en-US",
                   const std::string &defaultNS = "default",
                   const std::string &localeExtension = ".json");

  /**
   * @brief Get the I18n object instance contains all your configurations
   * @return i18n
   */
  static i18n &GetInstance();

  /**
   * @brief Set the current used locale
   */
  static void SetLocale(const std::string &locale);

  /**
   * @return The current used locale
   */
  static std::string GetLocale();

  /**
   * @brief Translate a message from it provided key
   * @param msgId Message identifier
   * @param args Arguments for value formating
   * @return The translated text
   */
  template <typename... Types>
  static const std::string Translate(const std::string &msgId, Types... args);

  /**
   * @brief Translate a message from it provided key.\n
   * Same as Translate() but with a namespace
   * @param nameSpace Namespace to use
   * @param msgId Message identifier
   * @param args Arguments for value formating
   * @return The translated text
   */
  template <typename... Types>
  static const std::string TranslateNS(const std::string &nameSpace,
                                       const std::string &msgId, Types... args);

private:
  i18n();
  ~i18n();

  std::filesystem::path m_localePath;
  std::string m_localeExtension;
  bool m_defaultLocale_exists;
  std::string m_defaultLocale;
  std::string m_locale;
  dictionary m_defaultDictionary;
  dictionary m_dictionary;
  std::string m_defaultNS;

  void ISetLocale(const std::string &locale);

  template <typename... Types>
  std::string ITranslate(const std::string &msgId, Types... args);

  template <typename... Types>
  std::string ITranslateNS(const std::string &nameSpace,
                           const std::string &msgId, Types... args);

  dictionary parseDictionary(std::filesystem::path locale_path);

  void loadDefaultDictionary();

  void loadDictionary(const std::string &locale);

  std::filesystem::path getLocalePath(const std::string &locale);
};

inline void i18n::Init(std::filesystem::path localePath,
                       const std::string &locale,
                       const std::string &defaultLocale,
                       const std::string &defaultNamespace,
                       const std::string &localeExtension) {
  GetInstance().m_localePath = localePath;
  GetInstance().m_defaultLocale = defaultLocale;
  GetInstance().m_locale = locale;
  GetInstance().m_defaultNS = defaultNamespace;
  GetInstance().m_localeExtension = localeExtension;
  GetInstance().loadDefaultDictionary();
  SetLocale(locale);
}

inline i18n &i18n::GetInstance() {
  static i18n instance;
  return instance;
}

inline void i18n::SetLocale(const std::string &locale) {
  GetInstance().ISetLocale(locale);
}

inline std::string i18n::GetLocale() { return GetInstance().m_locale; }

template <typename... Types>
inline const std::string i18n::Translate(const std::string &msgId,
                                         Types... args) {
  return GetInstance().ITranslate(msgId, std::forward<Types>(args)...);
}

template <typename... Types>
inline const std::string i18n::TranslateNS(const std::string &nameSpace,
                                           const std::string &msgId,
                                           Types... args) {
  return GetInstance().ITranslateNS(nameSpace, msgId,
                                    std::forward<Types>(args)...);
}

inline void i18n::ISetLocale(const std::string &locale) {
  this->m_locale = locale;
}

template <typename... Types>
inline std::string i18n::ITranslate(const std::string &msgId, Types... args) {
  if (sizeof...(Types) == 0) {
    if (!this->m_defaultLocale_exists) {
      if (this->m_locale == this->m_defaultLocale)
        return msgId;
      if (!this->m_dictionary[this->m_defaultNS].count(msgId))
        return msgId;
      return this->m_dictionary[this->m_defaultNS][msgId];
    }

    if (this->m_locale == this->m_defaultLocale)
      return this->m_defaultDictionary[this->m_defaultNS][msgId];
    if (!this->m_dictionary[this->m_defaultNS].count(msgId))
      return this->m_defaultDictionary[this->m_defaultNS][msgId];
    return this->m_dictionary[this->m_defaultNS][msgId];
  } else {
    if (!this->m_defaultLocale_exists) {
      if (this->m_locale == this->m_defaultLocale)
        return i18n_format(
            msgId, std::forward<Types>(args)...); // Current locale is the

      if (!m_dictionary[this->m_defaultNS].count(msgId))
        return i18n_format(
            msgId, std::forward<Types>(
                       args)...); // Tranlation doesn't exist in the locale file

      return i18n_format(
          this->m_dictionary[m_defaultNS][msgId],
          std::forward<Types>(
              args)...); // Return translated string from the dictionary
    }

    // If using a locale file for the default locale
    if (this->m_locale == this->m_defaultLocale)
      return i18n_format(
          this->m_defaultDictionary[this->m_defaultNS][msgId],
          std::forward<Types>(args)...); // Current locale is the default locale

    if (!this->m_dictionary[m_defaultNS].count(msgId))
      return i18n_format(
          this->m_defaultDictionary[this->m_defaultNS][msgId],
          std::forward<Types>(
              args)...); // Tranlation doesn't exist in the locale file

    return i18n_format(
        this->m_dictionary[this->m_defaultNS][msgId],
        std::forward<Types>(
            args)...); // Return translated string from the dictionary
  }
}

template <typename... Types>
inline std::string i18n::ITranslateNS(const std::string &ns,
                                      const std::string &msgid, Types... args) {
  if (sizeof...(Types) == 0) {
    if (!m_defaultLocale_exists) // If not using a locale file for the default
                                 // locale
    {
      if (m_locale == m_defaultLocale)
        return msgid; // Current locale is the default locale
      if (!m_dictionary[ns].count(msgid))
        return msgid; // Tranlation doesn't exist in the locale file
      return m_dictionary[ns][msgid]; // Return translated string from the
                                      // dictionary
    }
    // If using a locale file for the default locale
    if (m_locale == m_defaultLocale)
      return m_defaultDictionary[ns]
                                [msgid]; // Current locale is the default locale
    if (!m_dictionary[ns].count(msgid))
      return m_defaultDictionary[ns][msgid]; // Tranlation doesn't exist in the
                                             // locale file
    return m_dictionary[ns]
                       [msgid]; // Return translated string from the dictionary
  } else {
    if (!m_defaultLocale_exists) // If not using a locale file for the default
                                 // locale
    {
      if (m_locale == m_defaultLocale)
        return i18n_format(
            msgid, std::forward<Types>(
                       args)...); // Current locale is the default locale
      if (!m_dictionary[ns].count(msgid))
        return i18n_format(
            msgid, std::forward<Types>(
                       args)...); // Tranlation doesn't exist in the locale file
      return i18n_format(
          m_dictionary[ns][msgid],
          std::forward<Types>(
              args)...); // Return translated string from the dictionary
    }
    // If using a locale file for the default locale
    if (m_locale == m_defaultLocale)
      return i18n_format(
          m_defaultDictionary[ns][msgid],
          std::forward<Types>(args)...); // Current locale is the default locale
    if (!m_dictionary[ns].count(msgid))
      return i18n_format(
          m_defaultDictionary[ns][msgid],
          std::forward<Types>(
              args)...); // Tranlation doesn't exist in the locale file
    return i18n_format(
        m_dictionary[ns][msgid],
        std::forward<Types>(
            args)...); // Return translated string from the dictionary
  }
}

inline i18n::dictionary
i18n::parseDictionary(std::filesystem::path locale_path) {
  dictionary dictionary;
  enum lineType { NS, MSG_ID, MSG_STR };
  std::string line;
  lineType prev_type;
  std::ifstream file(locale_path);

  json object = json::parse(&file);

  for (auto it = object.begin(); it != object.end(); ++it) {
    if (it.value().type() == json::value_t::string) {
      dictionary[m_defaultNS][it.key()] = it.value().get<std::string>();
    }
  }
  file.close();
  return dictionary;
}

inline void i18n::loadDefaultDictionary() {
  std::filesystem::path defaultLocale_path = getLocalePath(m_defaultLocale);
  if (!std::filesystem::exists(defaultLocale_path)) {
    m_defaultLocale_exists = false;
    m_defaultDictionary.clear();
    return;
  }
  m_defaultLocale_exists = true;
  m_defaultDictionary = parseDictionary(defaultLocale_path);
}

inline void i18n::loadDictionary(const std::string &locale) {
  if (locale == m_defaultLocale) {
    m_dictionary.clear();
    return;
  }
  std::filesystem::path locale_path = getLocalePath(locale);
  if (!std::filesystem::exists(locale_path)) {
    m_locale = m_defaultLocale;
    m_dictionary.clear();
    return;
  }
  m_dictionary = parseDictionary(locale_path);
}

inline std::filesystem::path i18n::getLocalePath(const std::string &locale) {
  std::string locale_filename = locale + m_localeExtension;
  return (m_localePath / locale_filename);
}

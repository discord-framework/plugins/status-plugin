#pragma once

#include <discordframework/core/bot.h>
#include <discordframework/plugins-loader/plugin_info.h>

using DiscordFramework::Bot;
using DiscordFramework::PluginInfo;

class __attribute__((visibility("default"))) Plugin {
public:
  Plugin(Bot *bot, PluginInfo *info) : m_bot(bot), m_pInfo(info) {}
  virtual ~Plugin() = default;

  inline std::filesystem::path config_path() {
    size_t index = m_pInfo->name.find_last_of(".");
    std::string filename = m_pInfo->name.substr(0, index);

    return m_pInfo->path.parent_path() / (filename + ".json");
  }

protected:
  Bot *m_bot;
  PluginInfo *m_pInfo;
  std::map<std::string, dpp::event_handle> m_handlers{};

private:
};

typedef Plugin *(plugin_initfunctype)(Bot *, PluginInfo *);

/**
 * @brief A macro that lets us simply defnied the entrypoint of a plugin by name
 */
#define PLUGIN(plug_class_name)                                                \
  extern "C" __attribute__((visibility("default"))) Plugin *init_plugin(       \
      DiscordFramework::Bot *bot, DiscordFramework::PluginInfo *info) {        \
    return new plug_class_name(bot, info);                                     \
  }

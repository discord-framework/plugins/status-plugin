#pragma once

#include <discordframework/core/configuration.h>
#include <map>
#include <sharedlibs-loader/plugin.h>

class Status : public Plugin {
public:
  Status(Bot *bot, PluginInfo *info);
  ~Status();

  void onReady(const dpp::ready_t &event);
  void runThread();
  void updatePresence();

private:
  uint32_t m_index = 0;
  std::vector<std::string> m_status;
  bool m_terminate = false;
  std::unique_ptr<std::thread> m_thread;
  std::unique_ptr<DiscordFramework::Configuration> m_config;
};

PLUGIN(Status);

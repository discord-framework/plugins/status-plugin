#include "status.h"

#include <discordframework/utils/strings.h>
#include <nlohmann/json-schema.hpp>
#include <thread>

static json plugin_config = R"(
{
  "$schema": "https://json-schema.org/draft-07/schema#",
  "title": "Status plugin Configuration",
  "properties": {
    "status": {
      "type": "array",
      "items": {
        "type": "string"
      }
    }
  }
}
)"_json;

Status::Status(Bot *bot, PluginInfo *info) : Plugin(bot, info) {
  m_config = std::make_unique<DiscordFramework::Configuration>(
      this->config_path().string(),
      json{{"status", nlohmann::json::array({})}});

  try {
    nlohmann::json_schema::json_validator validator;

    validator.set_root_schema(plugin_config);
    validator.validate(*m_config->JSON());

    m_status = m_config->JSON()->at("status").get<std::vector<std::string>>();

    if (m_status.size() > 0) {
      auto h = m_bot->cluster->on_ready.attach(
          std::bind(&Status::onReady, this, std::placeholders::_1));

      m_handlers.insert({"ready", h});
    }
  } catch (const std::exception &e) {
    m_bot->cluster->log(
        dpp::ll_warning,
        fmt::format("[Status] Error while loading config", e.what()));
  }
}

Status::~Status() {
  if (!m_handlers.empty()) {
    this->m_terminate = true;
    m_bot->cluster->on_presence_update.detach(m_handlers["ready"]);
  }
}

void Status::onReady(const dpp::ready_t &event) {
  m_thread = std::make_unique<std::thread>(&Status::runThread, this);
}

void Status::runThread() {
  std::this_thread::sleep_for(std::chrono::seconds(10));

  if (m_status.size() > 1) {
    while (!this->m_terminate) {
      updatePresence();
      std::this_thread::sleep_for(std::chrono::seconds(30));
    }
  } else {
    updatePresence();
  }
}

void Status::updatePresence() {
  if (m_index >= m_status.size()) {
    m_index = 0;
  }

  auto txt = m_status.at(m_index);
  auto count = 0;

  for (auto &s : m_bot->cluster->get_shards()) {
    count += s.second->get_guild_count();
  }

  txt = Helpers::Strings::replaceString(txt, "%guild_count%",
                                        std::to_string(count));
  m_bot->cluster->set_presence(dpp::presence(dpp::presence_status::ps_online,
                                             dpp::activity_type::at_game, txt));

  m_index++;
}
